// console.log("Hello");

// ES6 Updates:

/*
	Exponent Operator:
		we use "**" for exponents
*/

const firstNum = Math.pow(8, 2);
console.log(firstNum);

const secondNum = 8 ** 2;
console.log(secondNum);

const thirdNum = 5 ** 5;
console.log(thirdNum);

// Template Literals

/*
	Allows us to write strings without using the concatination operator (+)
*/

let name = "George"

// Concatination / Pre-Template Literal
// Uing single Qoute (' ')

let message = 'Hello ' + name + ' Welcome to Zuiit Coding Bootcamp'
console.log("Message without template literal: " + message);

console.log(" ");

// Strings using template Literal

/*
	Uses the backticks (` `)
*/

message = `Hello ${name}. Welcome to Zuitt Coding Bootcamp`
console.log(`Message with template literal: ${message}`);

let anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8**2 with the solution of ${secondNum}
`
console.log(anotherMessage);

// Operation inside template literal
const interestRate = .1
const principal = 1000

console.log(`The interest on your savings is: ${principal * interestRate}`);

// Array Destructuring

/*
	Allows to unpack elements in an array into distinct variables. Allows us to name the array elements with variables instead of index number

	Syntax:
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Joe", "Dela", "Cruz"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

console.log(" ");

// Array Destructing
const [firstName, middleName, lastName] = fullName

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello, ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)

// Object Destructuring

/*
	Allows to unpack properties of objects into distinct variables. Shortens the syntax fro acessing properties from objects

	Syntax: 
		let/const {propertyName, propertyName, propertyName} = object
*/

console.log(" ");

const person = {
	givenName: "Jane",
	maindenName: "Dela",
	familyName: "Cruz"
}

// Pre-object Destructuring
console.log(person.givenName);
console.log(person.maindenName);
console.log(person.familyName);

function getFullName(givenName, maindenName, familyName) {
	console.log(`${givenName} ${maindenName} ${familyName}`)
}
getFullName(person.givenName, person.maindenName, person.familyName);

// Using Object Destructuring
const {maindenName, familyName, givenName} = person
console.log(person.givenName);
console.log(person.maindenName);
console.log(person.familyName);

function getFullName(givenName, maindenName, familyName) {
	console.log(`${givenName} ${maindenName} ${familyName}`)
}

getFullName(givenName, maindenName, familyName);

console.log(" ");

//MINI ACTIVITY

const employees = ["Taylor Swift", "Selena Gomez", "Gigi Hadid"]
console.log(employees[0]);
console.log(employees[1]);
console.log(employees[2]);

const[employee1, employee2, employee3] = employees
console.log(`Employees: ${employee1}, ${employee2}, ${employee3}`)

const pet = {
	pet1: "Mojo",
	pet2: "Magic",
	pet3: "Egypt"
}


console.log(pet.pet1);
console.log(pet.pet2);
console.log(pet.pet3);

const{pet1, pet2, pet3} = pet
console.log(`My pets: ${pet1}, ${pet2}, ${pet3}`);

console.log(" ");

// Pre-Arrow functions and Arrow function

/*
	Pre Arrow Function
	Syntax:
		function functionName(paramA, paramb) {
			statement // console.log // return
		}
*/

function printFullName(firstName, middleInitial, lastName) {
	console.log(firstName + " " + middleInitial + " " + lastName)
}

printFullName("Mae", "T", "Sargento");

/*
	 Arrow Function
	 Syntax:
	 	let/const variableName = (paramA, paramB) => {
			statement // console.log // return 
	 	}
*/

const printFullName1 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}

printFullName1("Marvin", "Paul", "Serrano");
console.log(" ");

const students = ["Yoby", "Emman", "Ronel"]

// FUNCTIONS WITH LOOP

// Pre-Arrow Function
students.forEach(function(student) {
	console.log(student + " is a student")
})
console.log(" ");

// Arrow Function

students.forEach((student) =>  {
	console.log(`${student} is a student`)
})

// Implicit Return Statement

// Pre- Arrow
function add(x, y) {
	return x + y
}

let total = add(12, 15)
console.log(total);

//Arrow
const addition = (x, y) => x + y


let total2 = addition(12, 15)
console.log(total2);


// Default Function Argument Value

const greet = (name = "User") => {
	return `Good evening, ${name}`
}

console.log(greet());
console.log(greet("Archie"));

// Class-Based Object Blueprint
/*
	Allows creation/instantiation of objects using class as blueprint

	Syntax:
		class className {
			constructor (objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA
				this.objectPropertyB = objectPropertyB
			}
		}
*/

class Car {
	constructor(brand, name, year) {
		this.brand = brand
		this.name = name
		this.year = year
	}
}

const myCar = new Car()
console.log(myCar);

myCar.brand = "Ford"
myCar.name = "Ranger Raptor"
myCar.year = 2021

console.log(myCar);

// const myNewCar = new Car("Toyota", "Vios", 2019)
// console.log(myNewCar);