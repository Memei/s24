// console.log("hello")

// S24 Activity Template:
	/*Item 1.)
		- Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)
		- Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
		*/

	// Code here:

	const getCube = 2 ** 3
	console.log(`The cube of 2 is ${getCube}`)


	/*Item 2.)
		- Create a variable address with a value of an array containing details of an address.
		- Destructure the array and print out a message with the full address using Template Literals.*/


	// Code here:

	const address = {
		street: "123 Lakisha St",
		city: "Houston",
		state: "Texas",
		zipCode: "12452"
	}

	const{street, city, state, zipCode} = address
	console.log(`I live at ${street} ${city} ${state} ${zipCode}`);
	


	/*Item 3.)
		- Create a variable animal with a value of an object data type with different animal details as its properties.
		- Destructure the object and print out a message with the details of the animal using Template Literals.
*/
	// Code here:
	const animal = {
		name: "Lolong",
		type: "saltwater crocodile",
		weight: "1075 kgs",
		measurement: "20 ft 3 in"
	}

	const{name, type, weight, measurement} = animal
	console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`);


	/*Item 4.)
		- Create an array of numbers.
		- Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.*/

	// Code here:

	const numbers = [1, 2, 3, 4, 5]

	numbers.forEach(function(number) {
		console.log(number)
	})

	function add(a, b, c, d, e) {
		return a + b + c + d + e
	}
	let total = add(1, 2, 3, 4, 5)
	console.log(total)


/*
	Item 5.)
		- Create a class of a Dog and a constructor that will accept a name, age and breed as its properties.
		- Create/instantiate a new object from the class Dog and console log the object.*/

	// Code here:

	class Dog {
		constructor(name, age, breed) {
			this.name = name
			this.age = age
			this.breed = breed
		}
	}

	const myDog = new Dog()
	myDog.name = "Chia"
	myDog.age = 2
	myDog.breed = "Corgi"
	console.log(myDog);